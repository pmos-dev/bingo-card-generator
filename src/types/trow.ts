import { CommonsType } from 'tscommons-core';

import { TCell, isTCell } from './tcell';

export type TRow = TCell[];

export function isTRow(test: unknown): test is TRow {
	if (!CommonsType.isArray(test)) return false;
	if (test.length !== 9) return false;
		
	for (const col of test) {
		if (!isTCell(col)) return false;
	}
	
	return true;
}
