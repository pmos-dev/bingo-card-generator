import { CommonsType } from 'tscommons-core';

import { TCell, isTCell } from './tcell';

export type TColumn = TCell[];

export function isTColumn(test: unknown): test is TColumn {
	if (!CommonsType.isArray(test)) return false;
	if (test.length !== 3) return false;
		
	for (const row of test) {
		if (!isTCell(row)) return false;
	}
	
	return true;
}
