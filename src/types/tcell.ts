import { CommonsType } from 'tscommons-core';

export type TCell = number|undefined;

export function isTCell(test: unknown): test is TCell {
	if (test === undefined) return true;
	if (CommonsType.isNumber(test)) return true;
	
	return false;
}
