import { CommonsType } from 'tscommons-core';

import { TRow, isTRow } from './trow';

export type TTicket = TRow[];

export function isTTicket(test: unknown): test is TTicket {
	if (!CommonsType.isArray(test)) return false;
	
	if (test.length !== 3) return false;
	
	if (!CommonsType.isTArray<TRow>(test, isTRow)) return false;
	
	return true;
}
