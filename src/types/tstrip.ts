import { CommonsType } from 'tscommons-core';

import { TTicket, isTTicket } from './tticket';

export type TStrip = TTicket[];

export function isTStrip(test: unknown): test is TStrip {
	if (!CommonsType.isArray(test)) return false;
	
	if (test.length !== 6) return false;
	
	if (!CommonsType.isTArray<TTicket>(test, isTTicket)) return false;
	
	return true;
}
