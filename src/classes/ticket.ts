import { CommonsArray } from 'tscommons-core';
import { CommonsNumber } from 'tscommons-core';

import { TStrip } from '../types/tstrip';
import { TTicket } from '../types/tticket';
import { TRow } from '../types/trow';
import { TColumn, isTColumn } from '../types/tcolumn';
import { TCell } from '../types/tcell';

type TPoolSet = number[];

// Based upon an algorithm description by Mark Henderson from:
// http://www.fractalforums.com/non-fractal-related-chit-chat/90-number-bingo-ticket-algorithm/

export class Ticket {
	public static renderTicket(ticket: TTicket): string {
		const lines: string[] = [];
		
		for (const row of ticket) {
			lines.push(row.map((cell: TCell): string => {
				if (cell === undefined) return '  ';
				
				let s: string = cell.toString(10);
				if (s.length === 1) s = ` ${s}`;
				
				return s;
			}).join(' '));
		}
		
		return lines.join('\n');
	}
	
	private static generatePoolSets(): TPoolSet[] {
		const poolSets: TPoolSet[] = [];
		
		for (let s = 0; s < 9; s++) {
			const start: number = s === 0 ? 1 : 0;
			const end: number = s === 8 ? 10 : 9;
			
			const set: TPoolSet = [];
			for (let unit = start; unit <= end; unit++) {
				const num: number = (s * 10) + unit;
				set.push(num);
			}
			
			CommonsArray.shuffle(set, 2);
			
			poolSets.push(set);
		}
		
		return poolSets;
	}
	
	private static generateEmptyRow(): TRow {
		const row: TRow = [];
		for (let c = 9; c-- > 0;) row.push(undefined);
		
		return row;
	}
	
	private static generateEmptyTicket(): TTicket {
		const ticket: TTicket = [];
		for (let r = 3; r-- > 0;) ticket.push(Ticket.generateEmptyRow());
		
		return ticket;
	}
	
	private static getTicketColumn(ticket: TTicket, s: number): TColumn {
		const column: TColumn = [];
		
		for (let r = 0; r < 3; r++) column.push(ticket[r][s]);
		
		if (!isTColumn(column)) throw new Error('Bad column generated');
		
		return column;
	}
	
	private static getExistingFilled(ticket: TTicket): number {
		let total: number = 0;
		
		for (const row of ticket) {
			for (const s of row) {
				if (s !== undefined) total++;
			}
		}
		
		return total;
	}
	
	private static generateValidTickets(ttl: number = 100000): TStrip {
		const poolSets: TPoolSet[] = Ticket.generatePoolSets();
		
		const tickets: TStrip = [];
		for (let i = 6; i-- > 0;) {
			const ticket: TTicket = Ticket.generateEmptyTicket();
			
			for (let s = 0; s < 9; s++) {
				ticket[0][s] = poolSets[s].pop();
			}
			
			tickets.push(ticket);
		}
		
		const lastTicketChoice: number = CommonsNumber.randRange(0, 3);
		tickets[lastTicketChoice][1][8] = poolSets[8].pop();
		
		for (let s = 0; s < 9; s++) {
			for (let i = 4; i-- > 0;) {
				if (ttl-- < 0) throw new Error('Unable to generate tickets within ttl');
				
				if (poolSets[s].length === 0) continue;
				
				const ticketChoice: number = CommonsNumber.randRange(0, 6);
				const ticket: TTicket = tickets[ticketChoice];

				const existingFilled: number = Ticket.getExistingFilled(ticket);
				if (existingFilled >= 15) {
					// this ticket is full
					i++;
					continue;
				}
				
				const columnCheck: TColumn = Ticket.getTicketColumn(ticket, s);
				
				const existingHeight: number = columnCheck
						.filter((row: TCell): boolean => row !== undefined)
						.length;
				
				if ((i > 0 && existingHeight > 1) || existingHeight > 2) {
					// aparently 2 works better than the logical 3 for the first 3 passes, as it prevents some kind of weird situation where the loop is infinite
					i++;
					continue;
				}
				
				ticket[existingHeight][s] = poolSets[s].pop();
			}
		}
		
		let check: number = 0;
		for (const s of poolSets) if (s.length > 0) check++;
		if (check > 0) throw new Error('The poolsets were not exhausted.');
		
		return tickets;
	}
	
	private static attemptGenerateValidTickets(
			generationCycleTtl: number = 100000,
			reattemptTtl: number = 20
	): TStrip {
		while (true) {
			if (reattemptTtl-- < 0) throw new Error('Unable to generate a valid tickets pass within ttl');
			
			try {
				const tickets: TStrip = Ticket.generateValidTickets(generationCycleTtl);
				return tickets;
			} catch (e) {
				continue;
			}
		}
	}
	
	private static distributeTicketNumbers(
			ticket: TTicket,
			distributionCycleTtl: number = 100000
	): void {
		// the top row is always the fullest, so we always need to push downwards, never raise upwards 
		for (let i = 0; i < 2; i++) {
			if (distributionCycleTtl-- < 0) throw new Error('Internal TTL for ticket distribution exceeded. Aborting. Try again.');
			
			let filled: number = 0;
			for (const t of ticket[i]) {
				if (t !== undefined) filled++;
			}

			if (filled < 5) throw new Error('Underflow');
			if (filled === 5) {
				// done this row
				continue;
			}
			
			while (true) {
				if (distributionCycleTtl-- < 0) throw new Error('Internal TTL for ticket distribution exceeded. Aborting. Try again.');
				
				const sChoice: number = CommonsNumber.randRange(0, 9);
				if (ticket[i][sChoice] === undefined) {
					// nothing here
					continue;
				}
				
				let space: number|undefined;
				for (let below = i + 1; below < 3; below++) {
					if (ticket[below][sChoice] === undefined) {
						space = below;
						break;
					}
				}
				
				if (space === undefined) {
					// no space; column must be full
					continue;
				}
				
				// shunt to the bottom
				ticket[space][sChoice] = ticket[i][sChoice];
				ticket[i][sChoice] = undefined;
				break;
			}
			
			i--;
		}
	}

	public static generateStrip(
			generationCycleTtl: number = 100000,
			reattemptTtl: number = 20,
			distributionCycleTtl: number = 100000
	): TStrip {
		const tickets: TStrip = Ticket.attemptGenerateValidTickets(
				generationCycleTtl,
				reattemptTtl
		);
		
		for (const ticket of tickets) {
			Ticket.distributeTicketNumbers(
					ticket,
					distributionCycleTtl
			);
		}
		
		return tickets;
	}
}
