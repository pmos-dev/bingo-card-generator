# Welcome to bingo-card-generator
![Version](https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000)
[![License: ISC](https://img.shields.io/badge/License-ISC-yellow.svg)](#)

> Simple generator for 90-ball bingo cards, in strips of 6 tickets.

Based upon an algorithm description by Mark Henderson from:
<http://www.fractalforums.com/non-fractal-related-chit-chat/90-number-bingo-ticket-algorithm/>

## Install

```sh
npm install
```

Exports a number of TypeScript types, most of which are used internally, and are just type-names for number arrays.

* TCell = number|undefined
* TRow = TCell[9]
* TColumn = TCell[3]
* TTicket = TRow[3]
* TStrip = TTicket[6]

Ticket gaps are represented by undefined; ticket numbers are regular numbers.

Ticket.generateStrip() is the main way to generate a strip of 6 tickets. As the tickets are generated iteratively, there is a chance the algorithm may get stuck in an impossible combination. Therefore there are TTLs employed to abort infinite loops. Default values are:

* generationCycleTtl: number = 100000,
* reattemptTtl: number = 20,
* distributionCycleTtl: number = 100000

I've found using these combinations strip generation is almost guaranteed to succeed. However, it is still possible for it to not, so be aware that you may need to catch exceptions.

## Author

👤 **Pete Morris**


## Show your support

Give a ⭐️ if this project helped you!


***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_